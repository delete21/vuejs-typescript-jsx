import { Component, Prop, Vue } from 'vue-property-decorator';
import { CreateElement, VNode } from 'vue';

import { ShowMessage } from './ShowMessage';


@Component
export default class HelloWorld extends Vue {
  @Prop() private msg!: string;

  public render(h: CreateElement): VNode {
    return (
      <ShowMessage
        msg={this.msg}
      />
    );
  }
}
