module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins: [
    'transform-vue-jsx',
    'module:@vue/babel-sugar-functional-vue'
  ]
}
